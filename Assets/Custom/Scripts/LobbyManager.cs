﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using VRTK;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public string startLevel = "";
    private VRTK_HeadsetFade headsetFade;
    public override void OnEnable()
    {
        base.OnEnable();
        headsetFade = GetComponent<VRTK_HeadsetFade>();
    }
    public override void OnDisable()
    {
        base.OnDisable();

    }
    public void StartFadeIn()
    {
        headsetFade.Fade(Color.black, .3f);

    }
    public void Connecting()
    {
        PhotonNetwork.NickName = "Player " + Random.Range(0,100);
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master");
        CreateOrJoinRoom();
    }
    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions {MaxPlayers = 2} );
    }
    public void CreateOrJoinRoom()
    {
        PhotonNetwork.JoinOrCreateRoom("Play_Room", new Photon.Realtime.RoomOptions {MaxPlayers = 20}, Photon.Realtime.TypedLobby.Default);

    }
    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }
    public override void OnJoinedRoom()
    {
        // Debug.Log("Joined to Room");
        // GetComponent<PhotonGameManager>()?.SpawnPlayer();

        // if(PhotonNetwork.IsMasterClient)
        // { 
            PhotonNetwork.LoadLevel(startLevel);
        // }
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log(message);
    }
}
