﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using VRTK;

public class PhotonPlayerController : MonoBehaviourPun, IPunObservable
{
    public VRTK_SDKObjectAlias headsetAlias;
    public PhotonView _photonView;
    public Transform leftHandModel, rightHandModel, Head;
    public Role userRole = Role.viewer;
    private VRTK_Pointer pointer;
    public CanvasController actorCanvas;
    public GameObject hoodie, mask;
    public Vector3 spawnPos;
    VRTK_HeadsetFade fade;
    public bool dev = false;
    void Start()
    {
        _photonView = _photonView == null ?  GetComponent<PhotonView>() : _photonView;

        if(_photonView.IsMine)
        {
            StartCoroutine(Setup());
        }
    }
    IEnumerator Setup()
    {
        yield return new WaitUntil( () =>  VRTK_DeviceFinder.PlayAreaTransform() != null );
        SetupUser();
        yield return new WaitForSecondsRealtime(.2f);
        if(fade) fade.Unfade(2f);
    }
    
    private void SwitchRole()
    {
        // userRole = RoleScript.instance.userRole;
        Debug.Log("switching role to " + userRole);
        switch (userRole)
        {
            // case Role.viewer:
            case Role.actor:
                // _photonView.RPC("SetupActor", RpcTarget.AllBufferedViaServer);    
                actorCanvas.followTransform = VRTK_DeviceFinder.HeadsetCamera();
                actorCanvas.gameObject.SetActive(true);            
                break;
            case Role.streamer:
                // _photonView.RPC("SetupStreamer", RpcTarget.AllBufferedViaServer);                
                break;
            default:
                // _photonView.RPC("SetupViewer", RpcTarget.AllBufferedViaServer);                
                break;
        }

    }
    [PunRPC]
    private void SetupActor()
    {
        userRole = RoleScript.instance.userRole;
        hoodie.SetActive(true);
        mask.SetActive(false);
        actorCanvas.followTransform = VRTK_DeviceFinder.HeadsetCamera();
        actorCanvas.gameObject.SetActive(true);
    }
    [PunRPC]
    private void SetupViewer()
    {
        userRole = RoleScript.instance.userRole;
        actorCanvas.gameObject.SetActive(false);
        hoodie.SetActive(false);
        mask.SetActive(true);
    }
    [PunRPC]
    private void SetupStreamer()
    {
        userRole = RoleScript.instance.userRole;
        hoodie.SetActive(false);
        mask.SetActive(false);
        actorCanvas.gameObject.SetActive(false);
    }
    private void SetupUser()
    {
        foreach(Renderer child in Head.GetComponentsInChildren<Renderer>())
        {
            child.GetComponent<Renderer>().enabled = false;
        }

        // _photonView.RPC("SwitchRole", RpcTarget.AllBuffered);
        SwitchRole();

        headsetAlias.enabled = true;
        leftHandModel.SetParent(VRTK_SDKManager.instance.scriptAliasLeftController.transform);
        rightHandModel.SetParent(VRTK_SDKManager.instance.scriptAliasRightController.transform);

        fade = FindObjectOfType<VRTK_HeadsetFade>();
        if(fade) fade.Fade(Color.black, 0f);
        
        VRTK_DeviceFinder.PlayAreaTransform().position = spawnPos;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            // stream.SendNext();
        }
        else
        {
            // stream.ReceiveNext();
        }
    }
    // bool IsConnected = false;
    // void Update()
    // {
    //     if(!IsConnected)
    //     {
    //         if(Input.GetKeyDown(KeyCode.LeftArrow))
    //         {

    //         }
    //     }
    // }
    // void OnTriggerEnter(Collider col)
    // {
    //     if(col.CompareTag("MainPlayer") && !IsConnected)
    //     {
    //         this.transform.SetParent(col.transform);
    //         IsConnected = true;
    //         this.tag = col.tag;
    //     }
    // }

}
