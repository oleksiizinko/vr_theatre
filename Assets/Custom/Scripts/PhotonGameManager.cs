﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using VRTK;


public enum Role {actor, viewer, streamer};
public class PhotonGameManager : MonoBehaviourPunCallbacks
{
    public static PhotonGameManager instance;
    void Awake()
    {
        instance = this;
    }
    // public GameObject userPrefab;
    public GameObject viewerPrefab, actorPrefab, streamerPrefab;
    public Transform actorSpawnPos, viewerSpawnPos, streamerSpawnPos;
    [HideInInspector]
    public Role userRole = Role.viewer;
    public float randomDelta = 2f;
    
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(gameObject.name + ": Player enter the room");
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log(gameObject.name + ": Player " + otherPlayer.NickName + " left the room");
    }
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
    public void SpawnPlayer()
    {
        if(RoleScript.instance) userRole = RoleScript.instance.userRole;
        // Debug.Log(userRole);
        Vector3 spawnPos = new Vector3();
        PhotonPlayerController player;

        switch (userRole)
        {
            case Role.viewer:
                spawnPos = RandomSpawnPos(viewerSpawnPos.position);
                player = PhotonNetwork.Instantiate(viewerPrefab.name, Vector3.zero, Quaternion.identity).GetComponent<PhotonPlayerController>();
                player.spawnPos = spawnPos;
                break;
            case Role.actor:
                spawnPos = RandomSpawnPos(actorSpawnPos.position);
                player = PhotonNetwork.Instantiate(actorPrefab.name, Vector3.zero, Quaternion.identity).GetComponent<PhotonPlayerController>();
                player.spawnPos = spawnPos;
                break;
            case Role.streamer:
                spawnPos = RandomSpawnPos(streamerSpawnPos.position);
                player = PhotonNetwork.Instantiate(streamerPrefab.name, Vector3.zero, Quaternion.identity).GetComponent<PhotonPlayerController>();
                player.spawnPos = spawnPos;
                break;
            default:
                return;
        }

        // PhotonPlayerController player = PhotonNetwork.Instantiate(userPrefab.name, Vector3.zero, Quaternion.identity).GetComponent<PhotonPlayerController>();
        // player.userRole = userRole;
        // player.spawnPos = spawnPos;
    }
    private Vector3 RandomSpawnPos(Vector3 pos)
    {
        Vector3 newPos = new Vector3(Random.Range(pos.x - randomDelta, pos.x + randomDelta), pos.y, Random.Range(pos.z - randomDelta, pos.z + randomDelta));
        return newPos;
    }
    void Start()
    {
        // GetComponent<VRTK_HeadsetFade>().Fade(Color.black, 0.1f);
        // Debug.Log("fade");
        SpawnPlayer();
    }
}
