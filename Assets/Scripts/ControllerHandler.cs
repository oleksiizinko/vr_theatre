﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

[RequireComponent(typeof(VRTK_BezierPointerRenderer))]
[RequireComponent(typeof(VRTK_UIPointer))]
[RequireComponent(typeof(VRTK_StraightPointerRenderer))]
[RequireComponent(typeof(VRTK_Pointer))]
public class ControllerHandler : MonoBehaviour
{
    public LayerMask layerCheckMask;
    public bool _disable = false;
    public bool enableTeleport = true;

    private VRTK_BezierPointerRenderer bezier;
    private VRTK_StraightPointerRenderer straight;
    private VRTK_UIPointer uiPointer;
    private VRTK_Pointer pointer;

    void Start()
    {
        bezier = GetComponent<VRTK_BezierPointerRenderer>();
        straight = GetComponent<VRTK_StraightPointerRenderer>();
        uiPointer = GetComponent<VRTK_UIPointer>();
        pointer = GetComponent<VRTK_Pointer>();
        UIPointerEnable(false);
    }

    public void UIPointerEnable(bool enable)
    {

        if(enable)
        {
            pointer.pointerRenderer = straight;
        }
        else
        {
           if(enableTeleport) pointer.pointerRenderer = bezier;
        }
        bezier.enabled = !enable;
        straight.enabled = enable;
        uiPointer.enabled = enable;
    }
    void Update()
    {
        RaycastHit hit;
        // Debug.DrawRay(transform.position, transform.forward * 100, Color.blue);
        // if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward)* 100, out hit, Mathf.Infinity, layerCheckMask))
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, layerCheckMask))
        {
            if(hit.transform.GetComponentInParent<VRTK_UICanvas>() != null && !_disable)
            {
                UIPointerEnable(true);
            }
            // Debug.Log(hit.transform.name);
            // else if(hit.transform.GetComponentInParent<VRTK_InteractableObject>() != null)
            //     Debug.Log("Not UI");
        }
        else 
        {
            UIPointerEnable(false);            
        }
    }
}
