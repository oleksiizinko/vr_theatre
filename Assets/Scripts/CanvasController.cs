﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasController : MonoBehaviour
{
    public Transform followTransform;
    [Range(0, 1)]
    public float smoothFactor = 0.5f;

// how far to stay away fromt he center

    public float offsetRadius   = 0.3f;
    public float distanceToHead = 4;
    
    public Toggle muteToggle;
    public TextMeshProUGUI textBlock;
    private int pageNum = 1;
    public void SwitchVoice(bool enable)
    {
        muteToggle.isOn = !enable;
    }
    public void ToggleVoice(Toggle muteToggle)
    {
        VoiceController.instance.EnableTransmit(!muteToggle.isOn);

    }
    public void SwitchPage(bool prev)
    {
        pageNum = prev ? pageNum-1 : pageNum+1;
        pageNum = Mathf.Clamp(pageNum, 1, textBlock.textInfo.pageCount);
        textBlock.pageToDisplay = pageNum;
        Debug.Log("page is " + pageNum + " " + textBlock.pageToDisplay);
        // textBlock.up
    }
    void Start()
    {
        SwitchVoice(false);
    }

    public void LateUpdate()
    {
        if(Input.GetKeyDown(KeyCode.V))
        {
            muteToggle.isOn = !muteToggle.isOn;
        }

        // make the UI always face towards the camera
        if(followTransform)
        {
            transform.rotation = followTransform.rotation;

            var cameraCenter = followTransform.position + followTransform.forward * distanceToHead;

            var currentPos = transform.position;

            // in which direction from the center?
            var direction = currentPos - cameraCenter;

            // target is in the same direction but offsetRadius
            // from the center
            var targetPosition = cameraCenter + direction.normalized * offsetRadius;

            // finally interpolate towards this position
            transform.position = Vector3.Lerp(currentPos, targetPosition, smoothFactor);

        }
    }
}
