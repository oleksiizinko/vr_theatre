﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoleScript : MonoBehaviour
{
    public static RoleScript instance;
    public Role userRole = Role.viewer;
    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    public void SetRole(InputField input)
    {
        switch (input.text)
        {
            case "1":
                userRole = Role.actor;
                break;
            case "2":
                userRole = Role.streamer;
                break;
            default:
                userRole = Role.viewer;
                break;
        }
    }
    
}
