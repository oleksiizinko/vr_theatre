﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class IKSetup : MonoBehaviour
{
    public Transform headTarget, leftHandTarget, rightHandTarget;

    void Start()
    {

    }
    IEnumerator SetupTargets()
    {
        yield return VRTK_DeviceFinder.PlayAreaTransform() != null;

        headTarget.SetParent(VRTK_DeviceFinder.HeadsetTransform());
        leftHandTarget.SetParent(VRTK_DeviceFinder.GetControllerLeftHand().transform);
        rightHandTarget.SetParent(VRTK_DeviceFinder.GetControllerRightHand().transform);
    }
}
