﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public GameObject console, vrConsole;
    public GameObject desktopUI, vrUI;
    private bool isSimulator = false;
    public bool dev = false;
    void Start()
    {
       if(!dev) StartCoroutine(SetupUI());
    }
    IEnumerator SetupUI()
    {
        yield return VRTK.VRTK_DeviceFinder.PlayAreaTransform() != null;

        isSimulator = VRTK.VRTK_DeviceFinder.GetHeadsetType() == VRTK.SDK_BaseHeadset.HeadsetType.Simulator;

        desktopUI.SetActive(isSimulator);
        vrUI.SetActive(!isSimulator);
        vrConsole.SetActive(false);
        console.SetActive(false);
    }

    public void SwitchConsole()
    {
        if(isSimulator)
        {
            console.SetActive(!console.activeInHierarchy);
        }
        else
        {
            vrConsole.SetActive(!vrConsole.activeInHierarchy);
        }
    }

    void Update()
    {
#if UNITY_STANDALONE || UNITY_EDITOR   
        if(Input.GetKeyDown(KeyCode.C)) SwitchConsole();
#endif
    }
}
