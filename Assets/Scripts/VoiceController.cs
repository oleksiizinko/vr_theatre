﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Voice.Unity;
using Photon.Voice.PUN;

public class VoiceController : MonoBehaviour
{
    public static VoiceController instance;

    public Recorder recorder;

    void Awake()
    {
        instance = this;
        EnableTransmit(false);
    }
    public void SwitchTransmit()
    {
        recorder.TransmitEnabled = !recorder.TransmitEnabled;
        
    }
    public void EnableTransmit(bool enable)
    {
        recorder.TransmitEnabled = enable;
    }
}
